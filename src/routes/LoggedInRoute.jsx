import keycloak from "../keycloak/keycloak";
import { NavLink } from 'react-router-dom';

function LoggedInRoute( { children } ){
    if (keycloak.authenticated ){ // Logged in
        return(
            <>{children}</>
        )
    } 
    
    else { // Not logged in
        return(
            <>
                <p>You must be logged in to view this page</p>
                <NavLink to="/">Return Home</NavLink>
            </>
        )
    }
}

export default LoggedInRoute;