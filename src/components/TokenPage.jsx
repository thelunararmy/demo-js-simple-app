import keycloak from "../keycloak/keycloak";
import "./TokenPage.css"

function TokenPage() {
    console.log(keycloak.tokenParsed)
    return(
        <>
            <h1>Token Page</h1>
            { keycloak.tokenParsed &&
                <div>
                    <span>User: {keycloak.tokenParsed.preferred_username}</span>
                    <pre>{keycloak.token}</pre>
                </div>
            }
        </>
    )
}
export default TokenPage;