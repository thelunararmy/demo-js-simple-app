import "./PicturePage.css"
import getImagePath from "../images/imageMap"

function PicturePage() {
    // Maybe fetch this from a state
    const images_to_render = ["b","a","d","f","f"]

    /* NOTE: This was a demonstration of mapping images from an Array demo. This is NOT the most effecient way of rendering images.
    Here is a much better approach:
    1. Store your images in a folder inside public called "images"
    2. To display the image simple set the src equal to the following path: "/images/<filename>.png"
    ... i.e.: <img src ="/images/01.PNG" alt = "gem"/>
    3. Use a mapping function to inject the filename you need into your component
    ... i.e.: ["01","02","03"].map( item => ( <img src ={`/images/${item}.PNG`} alt = "gem"/> ))

    You can see a great explanation of the public folder use here: https://stackoverflow.com/questions/47196800/reactjs-and-images-in-public-folder
    */

    return (
        <>
            <h3>My pictures</h3>
            {/* <img src={a} alt="my precious gem"></img> */}
            { images_to_render.map( (item,index) => { return(
                <img className="gem" src={ getImagePath(item) } alt={`gem${index}` }></img>
            )}) }
        </>
    )
}

export default PicturePage
