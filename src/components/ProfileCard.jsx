import { useContext } from "react"
import { ProfileContext } from "../hoc/ProfileContext"
import "./ProfileCard.css"

function ProfileCard() {
    const [profile,setProfile] = useContext(ProfileContext)
    
    const handleLogout = () => {
        setProfile(null)
    }
    
    return (
        <>
            { profile  && 
            <div className='ProfileCard'>
                <div className="item1"><img src={profile.picture} alt="profile" width="25px"/></div>
                <div className="item2"><span>{profile.username}</span></div>
                <div className="item3"><button onClick={ handleLogout }>Logout</button></div>
            </div> }
        </>
    )
}

export default ProfileCard