import { useForm } from "react-hook-form"

function SimpleForm() {

    const { register, handleSubmit, formState: { errors }} = useForm();

    const handleOnSubmit = (data) => {
        console.log(data)
    }

    return (
        <form onSubmit= { handleSubmit(handleOnSubmit) }>
            <input type="text" placeholder="nationality" {...register("nationality",{ required: true })} />
            <input type="text" placeholder="firstname" {...register("firstname", { minLength: 2 } )} />
            <input type="text" placeholder="surname" {...register("surname",{ required: true })} />
            <button type="submit">Submit Button</button>
            { errors.nationality && <p>Nationality is required</p>}
            { errors.firstname && <p>Error!</p>}
        </form>
    )
}

export default SimpleForm