import { useSelector, useDispatch } from "react-redux";
import { boost, decrement, increment, superboost } from "../reduxparts/actions";
import { getCount, getCountPositive } from "../reduxparts/selectors";

function CounterPage() {
    const count = useSelector(getCount)
    const count_positive = useSelector(getCountPositive)

    const dispatch = useDispatch()
    const handleIncrement = () => dispatch( increment() );
    const handleDecrement = () => dispatch( decrement() );
    const handleBoost = () => dispatch( boost(10) );
    const handleSuperBoost = () => dispatch( superboost() );

    return (
        <div>
            <h3>Counter: { count }</h3>
            <h3>The current count is</h3>{count_positive ? <p>Positive</p> : <p>Negative</p>}
            <button onClick={ handleIncrement }>+1</button>
            <button onClick={ handleDecrement }>-1</button>
            <button onClick={ handleBoost }>+10</button>
            <button onClick={ handleSuperBoost }>🚀</button>
        </div>
    )
}

export default CounterPage