import React, { useEffect, useState } from "react";
import UserProfileCard from "./UserProfileCard";

function UserProfile () {
    // Hooks
    const [ user, setUser ] = useState({});
    const [ data, setData ] = useState({});
    useEffect( () => {
        const fetchUser = async () => {
            try {
                const response = await fetch("https://randomuser.me/api/")
                const data = await response.json()
                const dataResults = data.results[0]
                const _userData = {
                    username: dataResults.login.username,
                    country: dataResults.location.country,
                    name: `${dataResults.name.title} ${dataResults.name.first} ${dataResults.name.last}`,
                    picture: dataResults.picture.thumbnail
                }
                setData(_userData)
            } catch (error) {
                console.log(error.message)
            }
        }
        fetchUser()
    }, [ user ]
    )

    // Handler
    const handleOnClickJC = (event) => {
        setUser({ username: "JC_Bailey" })
    }

    return(
        <>
            <p>User data</p>
            { user.username ? <p>{user.username}</p> : <p>No username</p> }
            <button onClick={ handleOnClickJC }>Set username to JC</button>
            <UserProfileCard userdata = { data } />
        </>
    )
}

export default UserProfile