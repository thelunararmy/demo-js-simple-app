# Strategy

1. Install Redux, Toolkit, Browser Extention
2. Create a store 
3. Create actions
4. Create reducer (based on actions)
5. Add reducer to the store
6. Test
7. Creating component
8. Adding in Selector to the component
9. Adding in Dispatch to the component
10. Go wild with actions
11. Makes some middlewear
12. Be happy
