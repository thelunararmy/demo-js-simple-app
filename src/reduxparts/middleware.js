import { boost } from "./actions";

export const countMiddleware = ({ dispatch }) => next => action => {
    next(action);

    // Logic to do extra stuff
    if (action.type === "[count] SUPER_BOOST") {
        setTimeout(() => {
            console.log("Charging hyperdrive, and GOGOGOGO!")
            dispatch( boost(9999) )
        },1000)
    }
}