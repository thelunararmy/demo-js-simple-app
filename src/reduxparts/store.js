import { composeWithDevTools } from "@redux-devtools/extension";
import { countMiddleware } from "./middleware";
import { countReducer } from "./reducers";

const { createStore, combineReducers, applyMiddleware } = require("redux");

const appReducer = combineReducers({
    count : countReducer
})

const middlewares = applyMiddleware(
    countMiddleware
)

const store = createStore(appReducer, composeWithDevTools(middlewares))

export default store