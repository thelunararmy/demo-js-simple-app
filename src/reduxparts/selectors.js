export const getCount = state => state.count;
export const getCountPositive = state => state.count > -1;
