import Keycloak from "keycloak-js";

const keycloak = new Keycloak("keycloak.json")

export const initialize = () => {
    const config = {
        onLoad: "check-sso",
        checkLoginIframe: false,
    }
    return keycloak.init(config)
}

export default keycloak;